/*
 * Stream.hpp
 *
 *  Created on: 1 дек. 2020 г.
 *      Author: avatar
 */

#ifndef STREAMBUF_STREAM_HPP_
#define STREAMBUF_STREAM_HPP_

#include "IStreambuf.hpp"
#include "BasicStreambuf.hpp"
#include "StreambufObj.hpp"
namespace Streams {




class Stream
{
public:

	Stream(uint32_t sizeElement,uint32_t numElement):
	reader(stream1),writer(stream2),vendorReader(stream2),venderWriter(stream1)
	,stream1(new BasicStreambuf(sizeElement,numElement))
	,stream2(new BasicStreambuf(sizeElement,numElement))

	{
	}
	Stream(IStreambufReadSection *userReader,IStreambufWriteSection *userWriter,
		   IStreambufReadSection *vendorReader,IStreambufWriteSection *vendorWriter)
		:reader(userReader),writer(userWriter),vendorReader(vendorReader),venderWriter(vendorWriter)
		{

		}

	virtual ~Stream(){};

	Streams::IStreambufReadSection *  const reader=0;
	Streams::IStreambufWriteSection * const writer=0;

protected:
	Streams::IStreambufReadSection *const vendorReader=0;
	Streams::IStreambufWriteSection *const venderWriter=0;

	Streams::IStreambuf *const stream1=0;
	Streams::IStreambuf *const stream2=0;
};

template<class T>
class StreamObj
{
public:

	StreamObj(uint32_t numElement):
	reader(stream1),writer(stream2),vendorReader(stream2),venderWriter(stream1)
	,stream1(new StreambufObj<T>(numElement))
	,stream2(new StreambufObj<T>(numElement))

	{

	}
//	StreamObj(IStreambufReadSection *userReader,IStreambufWriteSection *userWriter,
//		   IStreambufReadSection *vendorReader,IStreambufWriteSection *vendorWriter)
//		:reader(userReader),writer(userWriter),vendorReader(vendorReader),venderWriter(vendorWriter)
//		{
//
//		}

	virtual ~StreamObj(){};

	Streams::IStreambufReadSection *  const reader=0;
	Streams::IStreambufWriteSection * const writer=0;

protected:
	Streams::IStreambufReadSection *const vendorReader=0;
	Streams::IStreambufWriteSection *const venderWriter=0;

	Streams::IStreambuf *const stream1=0;
	Streams::IStreambuf *const stream2=0;
};

} //end namespace Stream
#endif /* STREAMBUF_STREAM_HPP_ */
