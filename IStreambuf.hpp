/*
 * IStreambuf.hpp
 *
 *  Created on: 30 нояб. 2020 г.
 *      Author: avatar
 */

#ifndef STREAM_ISTREAMBUF_HPP_
#define STREAM_ISTREAMBUF_HPP_

namespace Streams {

	class IStreambufCommon
	{
	public:
		virtual uint32_t getElementSize()=0;
		virtual uint32_t getMaxNumElementsInBuffer()=0;
		virtual uint32_t getFullSizeInByte()=0;
		virtual bool clear()=0;
		virtual bool isEmpty()=0;
		virtual bool isBroken()=0;
		virtual int getLastError()=0;
	};
class IStreambufWriteSection: public IStreambufCommon
{

public:
		virtual uint32_t getAvailableMemToWriteInByte()=0;
		virtual uint32_t getAvailableElementToWrite()=0;
		/**
			 * Запись данных в Write section
			 * @param data данные
			 * @param lenInByte кол-во байт для записи @attention ДОЛЖНЫ БЫТЬ КРАТНЫ РАЗМЕРУ ЭЛЕМЕНТА
			 * @return кол-во байт записанных. 0 если нет места для записи или <0 если все плохо
			 */
		virtual int32_t write(const void *data, uint16_t lenInByte)=0;
};
class IStreambufReadSection: public IStreambufCommon
{

public:
	virtual uint32_t getAvailableDataToReadInByte()=0;
	virtual uint32_t getAvailableElementToRead()=0;
	/**
		 *
		 * @param data буффер куда писать
		 * @param lenInByte кол-во байт для Чтения @attention ДОЛЖНЫ БЫТЬ КРАТНЫ РАЗМЕРУ ЭЛЕМЕНТА
		 * @return
	 */
	virtual int32_t read (void *data, uint16_t lenInByte)=0;
};
class IStreambuf:public IStreambufCommon , public IStreambufReadSection,public IStreambufWriteSection
{

};

}



#endif /* STREAM_ISTREAMBUF_HPP_ */
