/*
 * Streambuf.cpp
 *
 *  Created on: 30 нояб. 2020 г.
 *      Author: avatar
 */

#include "BasicStreambuf.hpp"
#include "string.h"
namespace Streams {

BasicStreambuf::BasicStreambuf(uint32_t sizeInByte) {
	m_buffer = new uint8_t[sizeInByte];
	m_elementSize = 1;
	m_numElement = sizeInByte;
	m_sizeInByte = sizeInByte;
	m_isEmpty = 1;
	m_isBroken = 0;
	p_begin = m_buffer;
	p_end = p_begin + sizeInByte;
	p_positionReadSection = p_begin;
	p_positionWriteSection = p_begin;

}

BasicStreambuf::BasicStreambuf(uint32_t sizeElementInByte,
		uint32_t numElement) {
	m_sizeInByte = sizeElementInByte * numElement;
	m_buffer = new uint8_t[m_sizeInByte];
	m_elementSize = sizeElementInByte;
	m_numElement = numElement;

	m_isEmpty = 1;
	m_isBroken = 0;
	p_begin = m_buffer;
	p_end = p_begin + m_sizeInByte;
	p_positionReadSection = p_begin;
	p_positionWriteSection = p_begin;
}

BasicStreambuf::BasicStreambuf(const BasicStreambuf &buf) {
	m_sizeInByte = buf.m_sizeInByte;
	m_buffer = new uint8_t[m_sizeInByte];
	m_elementSize = buf.m_elementSize;
	m_numElement = buf.m_numElement;

	m_isEmpty = buf.m_isEmpty;
	m_isBroken = buf.m_isBroken;
	p_begin = m_buffer;
	p_end = p_begin + m_sizeInByte;

	p_positionReadSection = p_begin;
	p_positionWriteSection = p_begin;
	if (!m_isEmpty) {
		if (buf.p_positionReadSection < buf.p_positionWriteSection) {
			memcpy(m_buffer, buf.p_positionReadSection,
					buf.p_positionWriteSection - buf.p_positionReadSection);
			p_positionWriteSection = p_positionWriteSection
					+ (buf.p_positionWriteSection - buf.p_positionReadSection);
		} else {
			memcpy(p_positionWriteSection, buf.p_positionReadSection,
					buf.p_end - buf.p_positionReadSection);
			p_positionWriteSection = p_positionWriteSection
					+ (buf.p_end - buf.p_positionReadSection);
			memcpy(p_positionWriteSection, buf.p_begin,
					buf.p_positionWriteSection - buf.p_begin);
			p_positionWriteSection = p_positionWriteSection
					+ (buf.p_positionWriteSection - buf.p_begin);
		}
	}

}

BasicStreambuf::~BasicStreambuf() {
	delete[] m_buffer;
}

int32_t BasicStreambuf::write(const void *data, uint16_t lenInByte) {
//	if(lenInByte>getAvailableMemToWriteInByte())
	if (data == 0)
		return NULL_POINTER_ERROR;
	if (lenInByte != m_elementSize)
		if (lenInByte % m_elementSize != 0) {
			return LEN_ELEMENT_ERROR;
		}
	auto free = getAvailableMemToWriteInByte();
	uint16_t len = lenInByte;
	if (free <= len)
		len = free;
	if (len == 0)
		return 0;

	if (p_positionWriteSection >= p_positionReadSection) {
		uint16_t len1 = p_end - p_positionWriteSection;
		if (len1 < len) {	//write to 2 segments
			memcpy(p_positionWriteSection, data, len1);
			memcpy(p_begin, (uint8_t*) data + len1, len - len1);
			p_positionWriteSection = p_begin + len - len1;
		} else {
			memcpy(p_positionWriteSection, data, len);
			p_positionWriteSection = p_positionWriteSection + len;
		}
	} else {

		memcpy(p_positionWriteSection, data, len);
		p_positionWriteSection = p_positionWriteSection + len;
	}
	m_isEmpty = 0;
	return len;
}

int32_t BasicStreambuf::write(BasicStreambuf &buf, uint16_t lenElement) {
	if (&buf == this)
		return 0;
	if ((m_elementSize != buf.m_elementSize)) {
		return LEN_ELEMENT_ERROR;
	}
	auto lenToWrite = lenElement * m_elementSize;//buf.getAvailableDataToReadInByte();
	if (lenToWrite > buf.getAvailableDataToReadInByte())
		lenToWrite = buf.getAvailableDataToReadInByte();

	auto lenFree = getAvailableMemToWriteInByte();
	auto len = lenToWrite;
	if (lenFree < lenToWrite)
		len = lenFree;
	if (p_positionWriteSection >= p_positionReadSection) {

		uint16_t len1 = p_end - p_positionWriteSection;
		if (len <= len1) {
			auto ret = buf.read(p_positionWriteSection, len);
			if (ret >= 0) {
				p_positionWriteSection = p_positionWriteSection + ret;
				if (ret != 0)
					m_isEmpty = 0;
			}
			return ret;
		} else {
			auto ret = buf.read(p_positionWriteSection, len1);
			if (ret == len1) //если перавя пачка норм записалась
					{
				auto ret = buf.read(p_begin, len - len1);
				if (ret >= 0)
					p_positionWriteSection = p_begin + ret;
				else {
					p_positionWriteSection = p_positionWriteSection + len1;

				}
				m_isEmpty = 0;
				return len1 + ret;
			} else {
				if (ret > 0) {
					p_positionWriteSection = p_positionWriteSection + ret;
				} else
					return ret;
			}
		}
	} else {
		auto ret = buf.read(p_positionWriteSection, len);
		p_positionWriteSection = p_positionWriteSection + len;
		if (ret > 0)
			m_isEmpty = 0;
		return ret;
	}
	return 0;

}

BasicStreambuf& BasicStreambuf::operator <<(BasicStreambuf &other) {
	 write(other,other.getAvailableElementToRead());
	 return *this;
}

int32_t BasicStreambuf::read(void *data, uint16_t lenInByte) {
	if (data == 0)
		return NULL_POINTER_ERROR;
	if (m_isEmpty)
		return 0;
	if (lenInByte != m_elementSize)
		if (lenInByte % m_elementSize != 0) {
			return LEN_ELEMENT_ERROR;
		}
	auto inbuf = getAvailableDataToReadInByte();
	uint16_t len = lenInByte;
	if (inbuf <= len)
		len = inbuf;
	if (p_positionWriteSection >= p_positionReadSection) {

		memcpy(data, p_positionReadSection, len);
		p_positionReadSection = p_positionReadSection + len;
	} else {
		uint16_t len1 = p_end - p_positionReadSection;
		if (len1 < len) {	//write to 2 segments
			memcpy(data, p_positionReadSection, len1);
			memcpy((uint8_t*) data + len1, p_begin, len - len1);
			p_positionReadSection = p_begin + len - len1;
		} else {
			memcpy(data, p_positionReadSection, len);
			p_positionReadSection = p_positionReadSection + len;
		}
	}
	if (p_positionReadSection == p_positionWriteSection)
		m_isEmpty = 1;

	return len;
}

BasicStreambuf& BasicStreambuf::operator >>(BasicStreambuf &other) {

	 read(other,getAvailableElementToRead());
	 return *this;
}

int32_t BasicStreambuf::read(BasicStreambuf &buf, uint16_t lenElement) {
	if (&buf == this)
		return 0;
	if ((m_elementSize != buf.m_elementSize)) {
		return LEN_ELEMENT_ERROR;
	}
	auto lenToRead = lenElement * m_elementSize;//buf.getAvailableDataToReadInByte();
	if (lenToRead > getAvailableDataToReadInByte())
		lenToRead = getAvailableDataToReadInByte();

	auto lenFree = buf.getAvailableMemToWriteInByte();
	auto len = lenToRead;
	if (lenFree < lenToRead)
		len = lenFree;
	if (p_positionReadSection >= p_positionWriteSection) {

		uint16_t len1 = p_end - p_positionReadSection;
		if (len <= len1) {
			auto ret = buf.write(p_positionReadSection, len);
			if (ret >= 0) {
				p_positionReadSection = p_positionReadSection + ret;
				if (p_positionReadSection == p_positionWriteSection)
					m_isEmpty = 1;
			}
			return ret;
		} else {

			auto ret = buf.write(p_positionReadSection, len1);
			if (ret == len1) //если перавя пачка норм записалась
					{
				auto ret = buf.write(p_begin, len - len1);
				if (ret >= 0)
					p_positionReadSection = p_begin + ret;
				else {
					p_positionReadSection = p_begin;
				}
				if (p_positionReadSection == p_positionWriteSection)
					m_isEmpty = 1;
				return len1 + ret;
			} else {
				if (ret > 0) {
					p_positionReadSection = p_positionReadSection + ret;
				} else
					return ret;
			}
		}
	} else {
		auto ret = buf.write(p_positionReadSection, len);
		if (ret >= 0)
		{
			p_positionReadSection = p_positionReadSection + ret;
		}
		return ret;
	}
	return 0;
}

}

bool Streams::BasicStreambuf::clear() {
	m_isEmpty = 1;
	p_positionWriteSection = p_begin;
	p_positionReadSection= p_begin;
	return true;
}
