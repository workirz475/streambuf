/*
 * StreamObj.hpp
 *
 *  Created on: 30 нояб. 2020 г.
 *      Author: avatar
 */

#ifndef STREAMBUF_STREAMBUFOBJ_HPP_
#define STREAMBUF_STREAMBUFOBJ_HPP_

#include "BasicStreambuf.hpp"
namespace Streams {

template<class T>
class StreambufObj:public BasicStreambuf {
public:
	StreambufObj(uint32_t len):BasicStreambuf(sizeof(T),len)
{

}
	virtual ~StreambufObj(){};
//	StreamObj(const StreamObj &other); //copy
//	StreamObj(StreamObj &&other);//move
//	StreamObj& operator=(const StreamObj &other);
//	StreamObj& operator=(StreamObj &&other);
	StreambufObj& operator<<( T &other)
	{
		auto ret = write(&other,sizeof(T));
		return *this;
	}
	StreambufObj& operator>>( T &other)
		{
			auto ret = read(&other,sizeof(T));
			return *this;
		}
};

} /* namespace Stream */

#endif /* STREAMBUF_STREAMBUFOBJ_HPP_ */
