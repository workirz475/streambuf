/*
 * Streambuf.hpp
 *
 *  Created on: 30 нояб. 2020 г.
 *      Author: avatar
 *
 *  Кольцевой буффер фиксированной длинны и постоянным размером элемента данных(по длине)
 *  с 2 раздельными интерфейсами на запись и на чтение.
 *
 *
 *  	|                             |
 *		^                             ^
 *		p_begin                       p_end
 *	Write section  	          ^
 *					p_positionWriteSection
 *	Read section		^
 *				p_positionReadSection
 */

#ifndef STREAM_BASICSTREAMBUF_HPP_
#define STREAM_BASICSTREAMBUF_HPP_

#include "stdint.h"
#include "IStreambuf.hpp"

namespace Streams {

class BasicStreambuf: public IStreambufCommon,
		public IStreambufReadSection,
		public IStreambufWriteSection,public IStreambuf {
public:

	enum eError: int32_t
	{
		NO_ERROR = 0,
		UNDEFINED_ERROR=-1,
		LEN_ELEMENT_ERROR = -2,
		NULL_POINTER_ERROR = -3,


	};

	BasicStreambuf(uint32_t sizeInByte);
	BasicStreambuf(uint32_t sizeElementInByte, uint32_t numElement);
	BasicStreambuf(const BasicStreambuf &buf);
	virtual ~BasicStreambuf();

	//Common Section
	virtual uint32_t getElementSize() {
		return m_elementSize;
	}
	virtual uint32_t getMaxNumElementsInBuffer() {
		return m_numElement;
	}
	virtual uint32_t getFullSizeInByte() {
		return m_elementSize * m_numElement;
	}
	virtual bool clear();
	virtual bool isEmpty() {
		return m_isEmpty;
	}
	virtual bool isBroken() {
		return m_isBroken;
	}
	virtual int getLastError() {
		return m_lastError;
	}
	//Write Section
	virtual uint32_t getAvailableMemToWriteInByte() {

		if (p_positionWriteSection > p_positionReadSection) {
			return ((p_end - p_positionWriteSection)
					+ (p_positionReadSection - p_begin));
		} else {
			return m_isEmpty?m_sizeInByte:(p_positionReadSection - p_positionWriteSection);
		}
	}
	virtual uint32_t getAvailableElementToWrite() {
		return getAvailableMemToWriteInByte()/m_elementSize;
	}

	/**
	 * Запись данных в Write section
	 * @param data данные
	 * @param lenInByte кол-во байт для записи @attention ДОЛЖНЫ БЫТЬ КРАТНЫ РАЗМЕРУ ЭЛЕМЕНТА
	 * @return кол-во байт записанных. 0 если нет места для записи или <0 если все плохо
	 */
	virtual int32_t write(const void *data, uint16_t lenInByte);
	virtual int32_t write(BasicStreambuf &buf, uint16_t lenElement);
	virtual BasicStreambuf& operator<<(BasicStreambuf &other);
	//Read Section
	virtual uint32_t getAvailableDataToReadInByte()
	{
	if (p_positionReadSection > p_positionWriteSection) {
			return ((p_end - p_positionReadSection)
					+ (p_positionWriteSection - p_begin));
		} else {
			return (p_positionWriteSection - p_positionReadSection);
		}
	}
	virtual uint32_t getAvailableElementToRead()
	{
		return getAvailableDataToReadInByte()/m_elementSize;
	}
	/**
	 * При чтении данные из буффера убираются, смещая указаетли read section
	 * @param data буффер куда писать
	 * @param lenInByte кол-во байт для Чтения @attention ДОЛЖНЫ БЫТЬ КРАТНЫ РАЗМЕРУ ЭЛЕМЕНТА
	 * @return кол-во байт записанных или -1 если все плохо
	 */
	virtual int32_t read(void *data, uint16_t lenInByte);
	virtual int32_t read(BasicStreambuf &buf, uint16_t lenElement);
	virtual BasicStreambuf& operator>>(BasicStreambuf &other);

protected:



	bool m_isEmpty = 0;
	bool m_isBroken = 0;
	int m_lastError = 0;
	uint32_t m_elementSize = 1;
	uint32_t m_numElement = 0;
	uint32_t m_sizeInByte = 0;

	uint8_t *p_begin = 0;
	uint8_t *p_end = 0;
	uint8_t *p_positionWriteSection = 0; // endReadSection = positionWriteSection

//	uint8_t *p_beginReadSection=0;
//	uint8_t *p_endReadSection=0;
	uint8_t *p_positionReadSection = 0;

	uint8_t *m_buffer = 0;

};

} /* namespace Stream */

#endif /* STREAM_BASICSTREAMBUF_HPP_ */
